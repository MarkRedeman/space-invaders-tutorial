var PlayerMissile = function(player) {
	this.width = 5;
	this.height = 15;
	this.x = player.x + player.width / 2 - this.width / 2;
	this.y = player.y - this.height;

	this.speed = 5;
};

PlayerMissile.prototype.update = function() {
	this.y -= this.speed;
};

PlayerMissile.prototype.draw = function(context) {
	context.rect(this.x, this.y, this.width, this.height);
	context.fill();
};