var Game = function() {
	this.fps = 60;
	// Load images
};

Game.prototype.initialize = function() {
	// Create player
	// Create aliens
	// Activate the update function every 1000 / fps seconds
	this.interval = window.setInterval(this.update, 1000 / this.fps);
};

Game.prototype.update = function() {
	// Update player
	// Update aliens
	this.redrawGame();
};

Game.prototype.redrawGame= function() {
	// Clear canvas
	// Draw player
	// Draw aliens
};

var game = new Game();