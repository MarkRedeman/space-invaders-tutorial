var Game = function() {
    this.canvas = document.getElementById('gameCanvas');
    this.context = this.canvas.getContext("2d");

    this.width = this.canvas.width;
    this.height = this.canvas.height;

    this.fps = 60;

    this.images = {};
    this.totalResources = 1;
    this.numResourcesLoaded = 0;
    // Load player image
    this.loadImage("player");
};

Game.prototype.loadImage = function(name) {
    this.images[name] = new Image();
    var that = this;
    this.images[name].onload = function() {
            that.resourceLoaded();
    };
    this.images[name].src = "images/" + name + ".png";
};

Game.prototype.resourceLoaded = function() {
    this.numResourcesLoaded += 1;
    if (this.numResourcesLoaded === this.totalResources) {
            var startButton = document.getElementById('start');
            startButton.innerText = 'Start game';
            startButton.addEventListener("click", function() {game.initialize();});
    }
};

Game.prototype.initialize = function() {
    // Create player
    // Create aliens
    // Activate the update function every 1000 / fps seconds
    this.interval = window.setInterval(this.update.bind(this), 1000 / this.fps);
};

Game.prototype.update = function() {
    // Update player
    // Update aliens
	this.redrawGame();
};

Game.prototype.redrawGame= function() {
	// Clear canvas
	// Draw player
	// Draw aliens
};

var game = new Game();
