var Invader = function(image, x, y) {
	this.image = image;
	this.height = this.image.height;
	this.width  = this.image.width;
	// Start at the bottom center of the game
	this.y = y;
	this.x = x;
	// Start moving right
	this.xDirection = 1;
	this.speed = 1;
};

Invader.prototype.draw = function(context) {
	context.drawImage(this.image, this.x, this.y);
};

Invader.prototype.update = function(moveDown) {
	if (moveDown) {
		this.xDirection *= -1;
		this.y += this.height / 2;
		this.speed = Math.min(2, this.speed + 0.05);
		// Check if invader has passed the player
		if (this.y > game.player.y) {
			game.gameover = true;
		}
	}
	this.x += this.xDirection * this.speed;
	// Check if invader passes the game border in the next frame if it keeps moving in the same direction
	return (this.x + this.speed * this.xDirection < 40) || (this.x + this.speed * this.xDirection > game.width - this.width - 40);
};

Invader.prototype.die = function() {
	this.dead = true;
};