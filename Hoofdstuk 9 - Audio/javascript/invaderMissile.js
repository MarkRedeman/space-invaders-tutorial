var InvaderMissile = function(invader) {
    this.width = 3;
    this.height = 10;
    this.x = invader.x + invader.width / 2 - this.width / 2;
    this.y = invader.y - this.height;

    this.speed = 2.5;
};

InvaderMissile.prototype.update = function() {
    this.y += this.speed;
    // Check if misisle hits player
    var player = game.player;
    if (this.collide(player)) {
        player.die();
        this.die();
        return;
    }
    // Else check if missile hits other missiles
    for (var i = 0; i < player.missiles.length; i++) {
        var missile = player.missiles[i];
        if (this.collide(missile)) {
            missile.die();
            this.die();
            return;
        }
    }
};

InvaderMissile.prototype.draw = function(context) {
    context.beginPath();
    context.rect(this.x, this.y, this.width, this.height);
    context.fill();
};

InvaderMissile.prototype.collide = function(target) {
    return !(
            (this.y + this.height < target.y)    ||
            (this.y > target.y + target.height) ||
            (this.x > target.x + target.width)  ||
            (this.x + this.width < target.x)
        );
};

InvaderMissile.prototype.die = function() {
    // Remove missile by removing it from the canvas
    this.y = game.height;
};