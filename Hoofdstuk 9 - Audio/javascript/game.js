var Game = function() {
    this.canvas = document.getElementById('gameCanvas');
    this.context = this.canvas.getContext("2d");

    this.width = this.canvas.width;
    this.height = this.canvas.height;

    this.fps = 60;

    this.images = {};
    this.sounds = {};
    this.totalResources = 5;
    this.numResourcesLoaded = 0;
    // Load images
    this.loadImage("player");
    this.loadImage("invader");
    // Load sounds
    this.loadSound('explosion');
    this.loadSound('invaderkilled');
    this.loadSound('shoot');
};

Game.prototype.loadImage = function(name) {
    this.images[name] = new Image();
    var that = this;
    this.images[name].onload = function() {
            that.resourceLoaded();
    };
    this.images[name].src = "images/" + name + ".png";
};

Game.prototype.loadSound = function(name) {
    this.sounds[name] = new Audio();
    var that = this;
    this.sounds[name].addEventListener('canplaythrough', function() {
        that.resourceLoaded();
    }, false);
    this.sounds[name].src = "sounds/" + name + ".wav";
};

Game.prototype.resourceLoaded = function() {
    this.numResourcesLoaded += 1;
    if (this.numResourcesLoaded === this.totalResources) {
            var startButton = document.getElementById('start');
            startButton.innerText = 'Start game';
            startButton.addEventListener("click", function() {game.initialize();});
    }
};

Game.prototype.initialize = function() {
    this.score = 0;
    this.level = 0;
    this.gameover = false;
    this.player = new Player(this.images['player']);
    this.invaders = [];
    this.missiles = [];
    // Create a grid of 40 invaders with 10 invaders per row
    this.generateInvaders();
    this.moveDown = false;
    // Activate the update function every 1000 / fps seconds
    this.interval = window.setInterval(this.update.bind(this), 1000 / this.fps);
};

Game.prototype.update = function() {
    if (this.gameover) {
        var startButton = document.getElementById('start');
        startButton.innerText = 'Try again';
        return this.endGame();
    }
    this.player.update();
    // Update invaders, check if the invaders should move down
    var previousMoveDown = this.moveDown;
    this.moveDown = false;
    for (var i = 0; i < this.invaders.length; i++) {
        if (this.invaders[i].dead) {
            this.invaders.splice(i, 1);
            continue;
        }
        // Check if invaders should move down at the next frame
        this.moveDown = this.invaders[i].update(previousMoveDown) || this.moveDown;
    }
    // Update invader missiles
    for (var i = 0; i < this.missiles.length; i++) {
        if (this.missiles[i].y > this.height) {
            this.missiles.splice(i, 1);
            continue;
        }
        this.missiles[i].update();
    }

    if (this.invaders.length === 0) {
        this.nextLevel();
    }

    this.redrawGame();
};

Game.prototype.redrawGame = function() {
    this.canvas.width = this.canvas.width; // clears the canvas

    this.player.draw(this.context);
    // Draw invaders
    for (var i = 0; i < this.invaders.length; i++) {
        this.invaders[i].draw(this.context);
    }
    // Draw invader missiles
    this.context.fillStyle = "red";
    for (var i = 0; i < this.missiles.length; i++) {
        this.missiles[i].draw(this.context);
    }
    this.drawInterface();
};

Game.prototype.drawInterface = function() {
    // Draw score
    this.context.fillStyle="#fff";
    this.context.lineStyle="#222";
    this.context.font="18px sans-serif";
    this.context.fillText("Score: " + this.score, 20, 570);
    this.context.fillText("Level: " + this.level, 20, 540);
};

Game.prototype.endGame = function() {
    window.clearInterval(this.interval);
    // To do: show gameover screen
};

Game.prototype.nextLevel = function() {
    // Clear all remaining missiles and generate a new Invader list
    this.missiles = [];
    this.player.missiles = [];
    this.generateInvaders();
    this.level++;
};

Game.prototype.generateInvaders = function() {
    // Create a grid of 40 invaders with 10 invaders per row
    for (var x = 0; x < 10;  x ++) {
        for (var y = 0; y < 5; y++) {
            this.invaders[this.invaders.length] = new Invader(
                this.images['invader'],
                40 + x * 80 + 24,
                y * 40 + 40
            );
        }
    }
};

var game = new Game();

