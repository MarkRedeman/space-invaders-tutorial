var PlayerMissile = function(player) {
	this.width = 5;
	this.height = 15;
	this.x = player.x + player.width / 2 - this.width / 2;
	this.y = player.y - this.height;

	this.speed = 5;
};

PlayerMissile.prototype.update = function() {
	this.y -= this.speed;
	for (var i = 0; i < game.invaders.length; i++) {
		if (this.collide(game.invaders[i])) {
			game.invaders[i].die();
			this.die();
			// The missile was destroyed, so we will stop updating
			return;
		}
	}
};

PlayerMissile.prototype.draw = function(context) {
    context.beginPath();
	context.rect(this.x, this.y, this.width, this.height);
	context.fill();
};

PlayerMissile.prototype.collide = function(target) {
    return !(
            (this.y + this.height < target.y)    ||
            (this.y > target.y + target.height) ||
            (this.x > target.x + target.width)  ||
            (this.x + this.width < target.x)
        );
};

PlayerMissile.prototype.die = function() {
    // Remove missile by removing it from the canvas
    this.y = -this.height;
};