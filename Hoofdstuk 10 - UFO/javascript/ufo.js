var UFO = function(image) {
    this.image = image;
    this.width = image.width;
    this.height = image.height;

    this.speed = 5;
    this.y = 20;

    // Randomly start from either the left or right border
    if (Math.random() < 0.5) {
        this.direction = 1;
    } else {
        this.direction = -1;
    }
    if (this.direction > 0) {
        this.x = 0 - this.width;
    } else {
        this.x = game.width;
    }
    game.sounds['ufo'].play();
};

UFO.prototype.update = function() {
    this.x = this.x + this.speed * this.direction;
};

UFO.prototype.draw = function(context) {
    context.drawImage(this.image, this.x, this.y);
};

UFO.prototype.die = function() {
    game.points += 50 + game.level * 10;
    this.loadSound('invaderkilled');
    // Remove UFO by removing it from the canvas
    if (this.direction > 0) {
        this.x = game.width;
    } else {
        this.x = - this.width;
    }
};