var Player = function(image) {
    this.image = image;
    this.height = this.image.height;
    this.width  = this.image.width;
    // Start at the bottom center of the game
    this.y = game.canvas.height - this.height - 10;
    this.x = game.canvas.width / 2 - this.width / 2;

    this.leftPressed = false;
    this.rightPressed = false;
    this.spacePressed = false;
    this.speed = 5;

    window.addEventListener('keydown', this.keydown.bind(this), true);
    window.addEventListener('keyup', this.keyup.bind(this), true);

    this.missiles = [];
    this.reloadTime = 30;
    this.reloading = 0;
    this.lives = 3;
};

Player.prototype.keydown = function(e) {
    switch (e.keyCode) {
        case 37: // left
            this.leftPressed = true;
            this.rightPressed = false;
            break;
        case 39: // right
            this.rightPressed = true;
            this.leftPressed = false;
            break;
        case 32: // Space
            this.spacePressed = true;
            break;
    }
};

Player.prototype.keyup = function(e) {
    switch (e.keyCode) {
        case 37: // left
            this.leftPressed = false;
            break;
        case 39: // right
            this.rightPressed = false;
            break;
        case 32: // Space
            this.spacePressed = false;
            break;
    }
};

Player.prototype.draw = function(context) {
    context.drawImage(this.image, this.x, this.y);
    this.drawLives(context);
    // Draw player's misslies
    context.fillStyle = "white";
    for (i = 0; i < this.missiles.length; i++) {
        this.missiles[i].draw(context);
    }
};

Player.prototype.drawLives = function(context) {
    // Draw  lives
    context.fillStyle="#fff";
    context.font="18px sans-serif";
    context.fillText("Lives: " + this.lives, 20, 600);
    // Draw the amount of lives the player has
    for (var i = 0; i < this.lives; i++) {
        context.drawImage(this.image, 20  + i * (this.width / 2 + 10), 620, this.width / 2, this.height / 2);
    }
};


Player.prototype.update = function() {
    if (this.leftPressed) {
        this.x = Math.max(40, this.x - this.speed);
    }
    if (this.rightPressed) {
        this.x = Math.min(game.width - this.width - 40, this.x + this.speed);
    }

    this.reloading = Math.max(0, this.reloading - 1);
    if (this.spacePressed && this.reloading === 0) { this.shoot(); }
    // Update player's missiles
    for (var i = 0; i < this.missiles.length; i++) {
        this.missiles[i].update();
        // Delete missile if missile is out of sight
        if (this.missiles[i].y + this.missiles[i].height < 0) {
            game.player.missiles.splice(i, 1);
        }
    }
};

Player.prototype.shoot = function() {
    this.reloading = this.reloadTime;
    this.missiles[this.missiles.length] = new PlayerMissile(this);
    game.sounds['shoot'].play();
};

Player.prototype.die = function() {
    if (this.lives > 1) {
        game.sounds['explosion'].play();
        this.lives--;
    } else {
        game.gameover = true;
    }
};